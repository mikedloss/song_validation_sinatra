# frozen_string_literal: true

require 'pry'
require_relative './pco'

# PCO Services class
class Services
  attr_accessor :songs, :arrangements

  ORDERING = "order=title&where[hidden]=false"

  def initialize
    @songs = []
    @connection = PCO.build_connection
    @initial_data = _get_initial_data
    _get_song_data
  end

  def all_arrangements
    @arrangements = @songs.map do |song|
      arrangement = {}
      arrangement['song'] = song.dig 'attributes', 'title'
      arrangement['song_id'] = song['id']
      arrangement_data = @connection.get("#{PCO::URL}services/v2/songs/#{song['id']}/arrangements?#{ORDERING}")
      arrangement['arrangements'] = arrangement_data.body['data']
      arrangement
    end
  end

  private

  def _get_initial_data
    @connection.get("#{PCO::URL}services/v2/songs? \
      #{ORDERING}&per_page=100&offset=0")
  end

  def _get_song_data
    songs_url = "#{PCO::URL}services/v2/songs? \
      #{ORDERING}&per_page=100&offset=0"
    loop do
      temp = @connection.get(songs_url)
      @songs = @songs.concat(temp.body['data'])
      break unless temp.body['links'].key?('next')

      songs_url = temp.body.dig 'links', 'next'
    end
  end
end
