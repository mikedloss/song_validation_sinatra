# frozen_string_literal: true

require 'dotenv/load'
require 'pry'
require 'sinatra'
require_relative './PCO/services'

before do
  content_type :json
end

get '/api/songs' do
  services = Services.new
  services.songs.to_json
end

get '/api/songs/arrangements' do
  p "Grabbing arrangements for #{request.ip}"
  services = Services.new
  services.all_arrangements.to_json
end
